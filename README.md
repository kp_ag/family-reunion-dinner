<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<div align="center">
    <img src="Title.jpg">
</div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">Family Reunion Dinner</h3>
  <p align="justify">
    A cooperative multiplayer game where players cook food for monsters by playing recipe, cooking, and ingredient cards. Players must work together to satisfy the monsters' preferences and requirements while dealing with limited communication and not being able to see their own cards.
    <br />
    <div align="center">
    <a href="https://youtu.be/7CtUZGuSjEY" target="_blank">Trailer</a>
    ·
    <a href="https://www.youtube.com/watch?v=Vr3fDGyfLqA" target="_blank">Gameplay demo</a>
    ·
    <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=2657426343&searchtext=dinner+with+nian" target="_blank">Tabletop Simulator Version</a>
    </div>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Family Dinner Reunion is a cooperative board game that involves strategic decision-making and careful planning. The game provides an engaging and challenging experience for players as they work together to satisfy the monsters' preferences and requirements.
The game's core gameplay revolves around playing cards to modify recipes and adding ingredients to complete them. The challenge lies in balancing the flavor and heat ranges, pot size, and monsters' preferences to earn points and avoid angering the diners. To win the game, players must complete a certain number of recipes with a specific number of points.
The game's level design includes various recipe cards with different flavor and heat ranges, pot sizes, and point values. Additionally, there are different types of ingredient cards that players can use to increase recipe points, such as meat, seafood, and vegetables. The game also includes skill cards that players can use to perform actions like trading cards, peeking at cards, or communicating secretly with their teammates.
The game's level of difficulty increases as the number of players increases. With more players, there are more cards to manage, and communication becomes more challenging. The game's design ensures that players must work together, rely on each other's skills, and think critically to succeed.

Project Goal: The goal of this project was to create a cooperative board game that challenges players to work together and communicate non-verbally. The game should be accessible to a wide audience and provide an enjoyable experience for players.

### Built With

Major frameworks/libraries used to bootstrap the project.

* Unreal 4.27

### Prerequisites

* Windows 10+
* Unreal 4.27


### Installation

1. Clone the repo

2. Open Project in Unreal

3. Play the Project inside Unreal or Make a Build

<!-- CONTACT -->
## Contact

* Kalpan Agrawal - agrawal.k@northeastern.edu
* [![LinkedIn][linkedin-shield]][linkedin-url]
* [![Portfolio][portfolioIcon-url]][portfolio-url]

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/kp-ag/
[portfolioIcon-url]: https://img.shields.io/badge/-Portfolio-brightgreen
[portfolio-url]: https://kalpan-ag.github.io/


